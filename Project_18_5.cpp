﻿#include <iostream>
using namespace std;

class Stack
{
public:
    void fillArray(int* const array, const int size)
    {
        for (int i = 0; i < size; i++)
        {
            array[i] = rand() % 100;
        }
    }

    void showArray(const int* const array, const int size, int arrayIndex)
    {
        if (arrayIndex == size-1)
        {
            cout << array[arrayIndex] << endl;
        }
        else
        {
            cout << array[arrayIndex++] << "\t";
            showArray(array, size, arrayIndex);
        }
    }

    void pushBack(int*& array, int& size, const int value)
    {
        int* newArray = new int[size + 1];

        for (int i = 0; i < size; i++)
        {
            newArray[i] = array[i];
        }

        newArray[size++] = value;

        delete[] array;

        array = newArray;
    }

    void popBack(int*& array, int& size)
    {
        size--;

        int* newArray = new int[size];

        for (int i = 0; i < size; i++)
        {
            newArray[i] = array[i];
        }

        delete[] array;

        array = newArray;
    }
};

int main()
{
    int size = 0, value = 0;
    cout << "Enter size: " << endl;
    cin >> size;

    int* array = new int[size];

    Stack stack;
    stack.fillArray(array, size);
    stack.showArray(array, size, 0);

    cout << "Enter new value: " << endl;
    cin >> value;

    cout << "Push" << endl;
    stack.pushBack(array, size, value);
    stack.showArray(array, size, 0);

    cout << "Pop" << endl;
    stack.popBack(array, size);
    stack.showArray(array, size, 0);

    delete[] array;
}